<?php

namespace App\Tests\Service;

use PHPUnit\Framework\TestCase;
use App\Service\AmountCalculator;
use App\Entity\Bordereau;
use App\Entity\Transaction;

class AmountCalculatorTest extends TestCase
{
    /** @var \DateTime $dateOfVisit */
    protected $dateOfTheDay;

    protected function setUp()
    {

        $this->dateOfTheDay = new \DateTime();
    }

    /**
     * @dataProvider dataPartSiege
     * @param $cotisation
     * @param $donSiege
     * @param $donSection
     * @param $hl
     * @param $ldhInfo
     * @param $expected
     */
    public function testPartSiege($cotisation, $donSiege, $donSection, $hl, $ldhInfo, $expected){
        $bordereau = new Bordereau();

        $transaction = $this->getTransaction($cotisation, $donSiege, $donSection, $hl, $ldhInfo);

        $bordereau->addTransaction($transaction);

        $calculator = new AmountCalculator();

        $this->assertEquals($expected, $calculator->partSiege($bordereau));
    }

    /**
     * @return array
     */
    public function dataPartSiege()
    {
        return [
            [10, 10, 10, 20, 10, 46.67],
            [10, 0, 10, 0, 0, 6.67],
            [0, 10, 0, 0, 0, 10],
            [0, 0, 0, 20, 10, 30],
        ];
    }


    /**
     * @dataProvider dataPartSection
     * @param $cotisation
     * @param $donSiege
     * @param $donSection
     * @param $hl
     * @param $ldhInfo
     * @param $expected
     */
    public function testPartSection($cotisation, $donSiege, $donSection, $hl, $ldhInfo, $expected){
        $bordereau = new Bordereau();

        $transaction = $this->getTransaction($cotisation, $donSiege, $donSection, $hl, $ldhInfo);

        $bordereau->addTransaction($transaction);

        $calculator = new AmountCalculator();

        $this->assertEquals($expected, $calculator->partSection($bordereau));
    }

    /**
     * @return array
     */
    public function dataPartSection()
    {
        return [
            [10, 10, 10, 20, 10, 3.33],
            [10, 0, 10, 0, 0, 3.33],
            [0, 10, 0, 0, 0, 0],
            [0, 0, 0, 20, 10, 0],
        ];
    }


    /**
     * @dataProvider dataTotalCotisation
     * @param $cotisation
     * @param $donSiege
     * @param $donSection
     * @param $hl
     * @param $ldhInfo
     * @param $expected
     */
    public function testTotalCotisation($cotisation, $donSiege, $donSection, $hl, $ldhInfo, $expected){
        $bordereau = new Bordereau();

        $transaction = $this->getTransaction($cotisation, $donSiege, $donSection, $hl, $ldhInfo);

        $bordereau->addTransaction($transaction);

        $calculator = new AmountCalculator();

        $this->assertEquals($expected, $calculator->totalCotisation($bordereau));
    }

    /**
     * @return array
     */
    public function dataTotalCotisation()
    {
        return [
            [10, 10, 10, 20, 10, 10],
            [10, 0, 10, 0, 0, 10],
            [0, 10, 0, 0, 0, 0],
            [0, 0, 0, 20, 10, 0],
        ];
    }

    /**
     * @param $cotisation
     * @param $donSiege
     * @param $donSection
     * @param $hl
     * @param $ldhInfo
     * @return Transaction
     */
    private function getTransaction($cotisation, $donSiege, $donSection, $hl, $ldhInfo) {
        $transaction = new Transaction();

        $transaction->setCotisation($cotisation);
        $transaction->setDonSiege($donSiege);
        $transaction->setDonSection($donSection);
        $transaction->setHl($hl);
        $transaction->setLdhInfo($ldhInfo);

        return $transaction;
    }
}
