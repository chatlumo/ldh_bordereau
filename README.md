Bordereaux de cotisation en ligne
======

## Installation de l'application
Le guide ci-dessous part du principe du serveur web déjà configuré.

Pré-requis : MySql et PHP 7.*

### Installation des sources et de la base de données
Se positionner dans le répertoire choisi pour installer l'application (/www).

1. Dans une console, saisir : 
`git clone https://chatlumo@bitbucket.org/chatlumo/ldh_bordereau.git`

2. Puis :
`cd ldh_bordereau`

3. Puis :
`composer install`

4. Saisir les paramètres demandés par la console

5. **Si la base de donnée n'a pas été créée en amont, saisir :**
`bin/console doctrine:database:create`

6. Puis :
`bin/console doctrine:schema:update --force`

7. Faire pointer le "document root" du serveur web vers le dossier "web" (ldh_bordereau/web).

8. Ajouter son adresse IP dans web/config.php et aller sur cette URL pour vérifier que l'environnement est prêt.

9. Ajouter un utilisateur Super Admin en console avec :
`bin/console fos:user:create`

10. Promouvoir l'utilisateur en Super Admin (ROLE_SUPER_ADMIN):
`bin/console fos:user:promote`

L'application est prête à être utilisée !

## Mise à jour de l'application
Pour mettre le code source de l'application, se rendre dans le dossier racine et faire :
1. `git pull origin master`
2. `bin/console cache:clear`
3. Vider le cache en mémoire : https://bordereaux.ldh-france.org/_resetCache.php

### Si mise à jour de la BDD
(1. `bin/console doctrine:migrations:diff` côté code)
2. `bin/console doctrine:migrations:migrate`

### Pour mettre à jour les sections
1. Déposer le fichier Export_tresoriers_pour_bordereau_en_ligne.txt dans le dossier "uploads"
2. Lancer `bin/console bordereaux:updatesections`
