<?php
namespace App\Command;

use App\Service\UpdateSections;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class UpdateSectionsCommand extends ContainerAwareCommand
{
    /**
     * @var UpdateSections
     */
    private $updater;

    public function __construct(UpdateSections $updater)
    {
        parent::__construct();
        $this->updater = $updater;
    }

    protected function configure()
    {
        $this
            ->setName('bordereaux:updatesections')
            ->setDescription('Importer les sections depuis un CSV')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set("memory_limit", "-1");

        $output->writeln($this->updater->update());
    }
}
