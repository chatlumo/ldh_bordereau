<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210331155600 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bordereau DROP FOREIGN KEY FK_F7B4C561A76ED395');
        $this->addSql('DROP INDEX IDX_F7B4C561A76ED395 ON bordereau');
        $this->addSql('ALTER TABLE bordereau DROP user_id');
        $this->addSql('ALTER TABLE section ADD username VARCHAR(180) NOT NULL, ADD username_canonical VARCHAR(180) NOT NULL, ADD email VARCHAR(180) NOT NULL, ADD email_canonical VARCHAR(180) NOT NULL, ADD salt VARCHAR(255) DEFAULT NULL, ADD password VARCHAR(255) NOT NULL, ADD last_login DATETIME DEFAULT NULL, ADD confirmation_token VARCHAR(180) DEFAULT NULL, ADD password_requested_at DATETIME DEFAULT NULL, ADD roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', CHANGE enabled enabled TINYINT(1) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEF92FC23A8 ON section (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFA0D96FBF ON section (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFC05FB297 ON section (confirmation_token)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bordereau ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE bordereau ADD CONSTRAINT FK_F7B4C561A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_F7B4C561A76ED395 ON bordereau (user_id)');
        $this->addSql('DROP INDEX UNIQ_2D737AEF92FC23A8 ON section');
        $this->addSql('DROP INDEX UNIQ_2D737AEFA0D96FBF ON section');
        $this->addSql('DROP INDEX UNIQ_2D737AEFC05FB297 ON section');
        $this->addSql('ALTER TABLE section DROP username, DROP username_canonical, DROP email, DROP email_canonical, DROP salt, DROP password, DROP last_login, DROP confirmation_token, DROP password_requested_at, DROP roles, CHANGE enabled enabled TINYINT(1) DEFAULT \'1\' NOT NULL');
    }
}
