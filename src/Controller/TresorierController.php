<?php

namespace App\Controller;

use App\Entity\Bordereau;
use App\Entity\Transaction;
use App\Entity\Section;
use App\Event\AddBordereauEvent;
use App\Event\BordereauEvents;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Manager\BordereauManager;
use App\Form\BordereauType;
use App\Form\TransactionType;

/**
 * Tresorier controller.
 *
 * @Route("tresorier")
 * @IsGranted("ROLE_TRESORIER")
 */
class TresorierController extends Controller
{
    /**
     * @Route("/", name="tresorier.homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route(
     *     "/createBordereau",
     *     name="createBordereau"
     * )
     * @param Request $request
     * @param BordereauManager $bordereauManager
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createBordereauAction(Request $request, BordereauManager $bordereauManager, EntityManagerInterface $entityManager)
    {
        //Step 1 : Informations du bordereau

        $user = $entityManager->getRepository(Section::class)->find($this->getUser()->getId());
        $bordereau = $bordereauManager->initBordereau();
        $bordereau->setSection($user);


        $form1 = $this->createForm(BordereauType::class, $bordereau);
        $form1->handleRequest($request);

        // If form datas OK, go to Step 2
        if ($form1->isSubmitted() && $form1->isValid()) {

            //$bordereauManager->saveBordereau($bordereau);

            return $this->redirectToRoute('addTransaction');
        }

        return $this->render('bordereau/createBordereau.html.twig', array(
            'form' => $form1->createView(),
        ));
    }

    /**
     * @Route(
     *     "/addTransaction",
     *     name="addTransaction"
     * )
     * @param Request $request
     * @param BordereauManager $bordereauManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addTransactionAction(Request $request, BordereauManager $bordereauManager)
    {
        //Step 2 : visitor informations
        $bordereau = $bordereauManager->getBordereau();

        if (null === $bordereau) {
            throw $this->createNotFoundException();
        }

        $transaction = new Transaction();
        $transaction->setBordereau($bordereau);

        $form = $this->createForm(TransactionType::class, $transaction);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $bordereau->addTransaction($transaction);

            return $this->redirectToRoute('addTransaction');
        }

        //dump($bordereau);

        return $this->render('bordereau/addTransaction.html.twig', array(
            'form' => $form->createView(),
            'bordereau' => $bordereau,
        ));
    }

    /**
     * @Route(
     *     "/editTransaction/{index}",
     *     name="transaction.edit"
     * )
     * @param $index
     * @param Request $request
     * @param BordereauManager $bordereauManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editTransactionAction($index, Request $request, BordereauManager $bordereauManager)
    {
        $bordereau = $bordereauManager->getBordereau();

        if (null === $bordereau) {
            throw $this->createNotFoundException();
        }

        $editTransaction = null;

        foreach ($bordereau->getTransactions() as $key => $transaction) {
            if ($key == $index) {
                $editTransaction = $transaction;
            }
        }

        if (null === $editTransaction) {
            throw $this->createNotFoundException();
        }


        $form = $this->createForm(TransactionType::class, $editTransaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //La modification est enregistrée automatiquement
            $this->addFlash('success', 'Ligne modifiée');

            return $this->redirectToRoute('addTransaction');
        }
        return $this->render('bordereau/editTransaction.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(
     *     "/removeTansaction/{index}",
     *     name="transaction.remove"
     * )
     * @param $index
     * @param Request $request
     * @param BordereauManager $bordereauManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function removeTransactionAction($index, Request $request, BordereauManager $bordereauManager)
    {
        $bordereau = $bordereauManager->getBordereau();

        if (null === $bordereau) {
            throw $this->createNotFoundException();
        }

        $removeTransaction = null;

        foreach ($bordereau->getTransactions() as $key => $transaction) {
            if ($key == $index) {
                $removeTransaction = $transaction;
            }
        }

        if (null === $removeTransaction) {
            throw $this->createNotFoundException();
        }

        $bordereau->removeTransaction($removeTransaction);

        $this->addFlash('notice', 'Ligne supprimée');

        return $this->redirectToRoute('addTransaction');
    }

    /**
     * @Route(
     *     "/sendBordereau/",
     *     name="sendBordereau"
     * )
     * @param Request $request
     * @param BordereauManager $bordereauManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendBordereauAction(Request $request, BordereauManager $bordereauManager, EntityManagerInterface $entityManager)
    {
        $bordereau = $bordereauManager->getBordereau();

        if (null === $bordereau) {
            throw $this->createNotFoundException();
        }

        $user = $entityManager->getRepository(Section::class)->find($this->getUser()->getId());

        if (count($bordereau->getTransactions()) > 0) {
            $bordereau = $bordereauManager->saveBordereau($bordereau,$user);
            // On crée l'événement
            $event = new AddBordereauEvent($bordereau);
            // On déclenche l'évènement
            $this->get('event_dispatcher')->dispatch(BordereauEvents::ADD_BORDEREAU, $event);
        } else {
            throw $this->createNotFoundException();
        }

        //return $this->redirectToRoute('homepage');
        return $this->render('bordereau/sendBordereau.html.twig', array(
            'bordereau' => $bordereau,
        ));
    }

    /**
     * @Route(
     *     "/listeBordereaux",
     *     name="listeBordereaux"
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function listeBordereauxAction(Request $request)
    {

        $user = $this->getUser();
        $bordereaux = $this->getDoctrine()
            ->getRepository(Bordereau::class)
            ->findBy(
                array('section' => $user),
                array('dateSaisie' => 'DESC')
            );

        return $this->render('bordereau/listeBordereaux.html.twig', array(
            'bordereaux' => $bordereaux,
        ));
    }

    /**
     * @Route(
     *     "/voirBordereau/{index}",
     *     name="voirBordereau",
     *     requirements={"index" = "\d+"}
     * )
     * @param Request $request
     * @param $index
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function voirBordereauAction(Request $request, $index)
    {
        $bordereau = $this->getBordereau($index);

        return $this->render('bordereau/voirBordereau.html.twig', array(
            'bordereau' => $bordereau,
        ));
    }

    /**
     * @Route("/exporterBordereau/{index}.{_format}",
     *     name="exporterBordereau",
     *     defaults={"_format"="xls"},
     *     requirements={"index" = "\d+", "_format"="csv|xls|xlsx"},
     * )
     * @param $index
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportBordereauAction($index)
    {
        $bordereau = $this->getBordereau($index);

        return $this->render('bordereau/spreadsheet.twig', array(
            'bordereau' => $bordereau,
        ));
    }

    /**
     * @param $index
     * @return Bordereau|null|object
     */
    private function getBordereau($index)
    {
        $user = $this->getUser();

        $bordereau = $this->getDoctrine()
            ->getRepository(Bordereau::class)
            ->find($index);

        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            if ( (null === $bordereau) || ($bordereau->getSection() !== $user) ) {
                throw $this->createNotFoundException();
            }
        }

        return $bordereau;
    }
}
