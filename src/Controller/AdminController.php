<?php

namespace App\Controller;

use App\Service\UpdateSections;
use App\Service\UpdateProfessions;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Bordereau;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Admin controller.
 *
 * @Route("admin")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController extends Controller
{
    /**
     * @Route("/update", name="admin_update_sections_users")
     * @param UpdateSectionsUsers $update
     * @return Response
     */
    public function indexAction()
    {
        //$datas = $update->import();

        return $this->render('default/index.html.twig');
        //return new Response($datas);
    }

    /**
     * @Route("/bordereaux/{type}/{page}",
     *     name="admin.bordereaux",
     *     defaults={"type"="tous", "page" = 1},
     *     requirements={"type" = "\w*", "type"="tous|nonexportes|nonexportescompta|nonexportesprodon|nonexportesplvt|exportes", "page" = "\d+"},
     *     )
     * @param $type
     * @param $page
     * @return Response
     */
    public function bordereauxListeAction($type, $page)
    {
        $nbBordereauxParPage = 20;

        switch ($type) {
            case 'nonexportes':
                $bordereaux = $this->getDoctrine()
                    ->getRepository(Bordereau::class)
                    ->findNonExportes($page, $nbBordereauxParPage);
                break;

            case 'nonexportescompta':
                $bordereaux = $this->getDoctrine()
                    ->getRepository(Bordereau::class)
                    ->findNonExportesCompta($page, $nbBordereauxParPage);
                break;

            case 'nonexportesprodon':
                $bordereaux = $this->getDoctrine()
                    ->getRepository(Bordereau::class)
                    ->findNonExportesProdon($page, $nbBordereauxParPage);
                break;

            case 'nonexportesplvt':
                $bordereaux = $this->getDoctrine()
                    ->getRepository(Bordereau::class)
                    ->findNonExportesPlvt($page, $nbBordereauxParPage);
                break;

            case 'exportes':
                $bordereaux = $this->getDoctrine()
                    ->getRepository(Bordereau::class)
                    ->findExportes($page, $nbBordereauxParPage);
                break;

            default:
                $bordereaux = $this->getDoctrine()
                    ->getRepository(Bordereau::class)
                    ->findAllByDate($page, $nbBordereauxParPage);
        }

        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($bordereaux) / $nbBordereauxParPage),
            'nomRoute' => 'admin.bordereaux',
            'paramsRoute' => array(
                'type' => $type
            )
        );

        return $this->render('admin/bordereauxListe.html.twig', array(
            'bordereaux' => $bordereaux,
            'type' => $type,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/bordereaux_prodon",
     *     name="admin.exporter.bordereaux",
     * )
     *
     */
    /*
    public function exporterBordereauxAction()
    {
        $response = new StreamedResponse();
        $response->setCallback(function() {
            $handle = fopen('php://output', 'w+');

            $updatedDate = new \DateTime();

            $bordereaux = $this->getDoctrine()
                ->getRepository(Bordereau::class)
                ->findNonExportes('ASC');

            // Add the header of the CSV file
            fputcsv($handle, array_map(array($this, 'toWin'), array(
                'NoClient',
                'CliAppellation',
                'CliNom',
                'CliPrenom',
                'CliAdresse',
                'CliCodePostal',
                'CliVille',
                'CliTelephoneResidence',
                'CliCourriel',
                'CliDateNaissance',
                'TrxDateDon',
                'TrxCodeOccasion',
                'TrxCodeActivite',
                'TrxMontantDon',
                'TrxQuantite',
                'TrxModePaiement',
                'TrxReferencePaiement',
                'TrxReçuAnnuel',
                'TrxReçuStatut',
                'TrxClientLie1NoClient',
                'TrxNote'
            )));



            foreach ($bordereaux as $bordereau) {
                foreach ($bordereau->getTransactions() as $trx) {
                    if ( !empty($trx->getCotisation()) ) {
                        $arrCot = array();
                        $arrCot[] = $trx->getAdherent()->getNoAdh();
                        $arrCot[] = $trx->getAdherent()->getCivilite();
                        $arrCot[] = $trx->getAdherent()->getNom();
                        $arrCot[] = $trx->getAdherent()->getPrenom();
                        $arrCot[] = str_replace(array("\n", "\r"), array("##", ""), $trx->getAdherent()->getAdresse());
                        $arrCot[] = $trx->getAdherent()->getCodePostal();
                        $arrCot[] = $trx->getAdherent()->getVille();
                        $arrCot[] = $trx->getAdherent()->getTelephone();
                        $arrCot[] = $trx->getAdherent()->getEmail();
                        $arrCot[] = empty($trx->getAdherent()->getDateNaissance()) ? "" : $trx->getAdherent()->getDateNaissance()->format("d/m/Y");
                        $arrCot[] = $bordereau->getDateSaisie()->format("d/m/Y");
                        $arrCot[] = "ADH" . $bordereau->getAnneeCotis();
                        $arrCot[] = "VIA SECTION";
                        $arrCot[] = $trx->getCotisation();
                        $arrCot[] = "";
                        $arrCot[] = "CH";
                        $arrCot[] = "B-" . $bordereau->getSection()->getCompteComptable() . "-" . $bordereau->getId();
                        $arrCot[] = ($trx->getCotisation() > 0) ? "O" : "N";
                        $arrCot[] = ($trx->getCotisation() > 0) ? "O" : "N";
                        $arrCot[] = $bordereau->getSection()->getId();
                        $arrCot[] = $trx->getNote();

                        fputcsv($handle, array_map(array($this, 'toWin'), $arrCot));
                    }
                    if ( $trx->getDonSiege() > 0 ) {
                        $arrCot = array();
                        $arrCot[] = $trx->getAdherent()->getNoAdh();
                        $arrCot[] = $trx->getAdherent()->getCivilite();
                        $arrCot[] = $trx->getAdherent()->getNom();
                        $arrCot[] = $trx->getAdherent()->getPrenom();
                        $arrCot[] = str_replace(array("\n", "\r"), array("##", ""), $trx->getAdherent()->getAdresse());
                        $arrCot[] = $trx->getAdherent()->getCodePostal();
                        $arrCot[] = $trx->getAdherent()->getVille();
                        $arrCot[] = $trx->getAdherent()->getTelephone();
                        $arrCot[] = $trx->getAdherent()->getEmail();
                        $arrCot[] = empty($trx->getAdherent()->getDateNaissance()) ? "" : $trx->getAdherent()->getDateNaissance()->format("d/m/Y");
                        $arrCot[] = $bordereau->getDateSaisie()->format("d/m/Y");
                        $arrCot[] = "DON SECTION";
                        $arrCot[] = "DON SIEGE";
                        $arrCot[] = $trx->getDonSiege();
                        $arrCot[] = "";
                        $arrCot[] = "CH";
                        $arrCot[] = "B-" . $bordereau->getSection()->getCompteComptable() . "-" . $bordereau->getId();
                        $arrCot[] = ($trx->getDonSiege() > 0) ? "O" : "N";
                        $arrCot[] = ($trx->getDonSiege() > 0) ? "O" : "N";
                        $arrCot[] = $bordereau->getSection()->getId();
                        $arrCot[] = $trx->getNote();

                        fputcsv($handle, array_map(array($this, 'toWin'), $arrCot));
                    }
                    if ( $trx->getDonSection() > 0 ) {
                        $arrCot = array();
                        $arrCot[] = $trx->getAdherent()->getNoAdh();
                        $arrCot[] = $trx->getAdherent()->getCivilite();
                        $arrCot[] = $trx->getAdherent()->getNom();
                        $arrCot[] = $trx->getAdherent()->getPrenom();
                        $arrCot[] = str_replace(array("\n", "\r"), array("##", ""), $trx->getAdherent()->getAdresse());
                        $arrCot[] = $trx->getAdherent()->getCodePostal();
                        $arrCot[] = $trx->getAdherent()->getVille();
                        $arrCot[] = $trx->getAdherent()->getTelephone();
                        $arrCot[] = $trx->getAdherent()->getEmail();
                        $arrCot[] = empty($trx->getAdherent()->getDateNaissance()) ? "" : $trx->getAdherent()->getDateNaissance()->format("d/m/Y");
                        $arrCot[] = $bordereau->getDateSaisie()->format("d/m/Y");
                        $arrCot[] = "DON SECTION";
                        $arrCot[] = "DON SECTION";
                        $arrCot[] = $trx->getDonSection();
                        $arrCot[] = "";
                        $arrCot[] = "CH";
                        $arrCot[] = "B-" . $bordereau->getSection()->getCompteComptable() . "-" . $bordereau->getId();
                        $arrCot[] = ($trx->getDonSection() > 0) ? "O" : "N";
                        $arrCot[] = ($trx->getDonSection() > 0) ? "O" : "N";
                        $arrCot[] = $bordereau->getSection()->getId();
                        $arrCot[] = $trx->getNote();

                        fputcsv($handle, array_map(array($this, 'toWin'), $arrCot));
                    }
                    if ( $trx->getHl() > 0 ) {
                        $arrCot = array();
                        $arrCot[] = $trx->getAdherent()->getNoAdh();
                        $arrCot[] = $trx->getAdherent()->getCivilite();
                        $arrCot[] = $trx->getAdherent()->getNom();
                        $arrCot[] = $trx->getAdherent()->getPrenom();
                        $arrCot[] = str_replace(array("\n", "\r"), array("##", ""), $trx->getAdherent()->getAdresse());
                        $arrCot[] = $trx->getAdherent()->getCodePostal();
                        $arrCot[] = $trx->getAdherent()->getVille();
                        $arrCot[] = $trx->getAdherent()->getTelephone();
                        $arrCot[] = $trx->getAdherent()->getEmail();
                        $arrCot[] = empty($trx->getAdherent()->getDateNaissance()) ? "" : $trx->getAdherent()->getDateNaissance()->format("d/m/Y");
                        $arrCot[] = $bordereau->getDateSaisie()->format("d/m/Y");
                        $arrCot[] = "ABO" . $bordereau->getAnneeCotis();
                        $arrCot[] = "HL ADHERENTS";
                        $arrCot[] = $trx->getHl();
                        $arrCot[] = "1";
                        $arrCot[] = "CH";
                        $arrCot[] = "B-" . $bordereau->getSection()->getCompteComptable() . "-" . $bordereau->getId();
                        $arrCot[] = "N";
                        $arrCot[] = "N";
                        $arrCot[] = "";
                        $arrCot[] = $trx->getNote();

                        fputcsv($handle, array_map(array($this, 'toWin'), $arrCot));
                    }
                    if ( $trx->getLdhInfo() > 0 ) {
                        $arrCot = array();
                        $arrCot[] = $trx->getAdherent()->getNoAdh();
                        $arrCot[] = $trx->getAdherent()->getCivilite();
                        $arrCot[] = $trx->getAdherent()->getNom();
                        $arrCot[] = $trx->getAdherent()->getPrenom();
                        $arrCot[] = str_replace(array("\n", "\r"), array("##", ""), $trx->getAdherent()->getAdresse());
                        $arrCot[] = $trx->getAdherent()->getCodePostal();
                        $arrCot[] = $trx->getAdherent()->getVille();
                        $arrCot[] = $trx->getAdherent()->getTelephone();
                        $arrCot[] = $trx->getAdherent()->getEmail();
                        $arrCot[] = empty($trx->getAdherent()->getDateNaissance()) ? "" : $trx->getAdherent()->getDateNaissance()->format("d/m/Y");
                        $arrCot[] = $bordereau->getDateSaisie()->format("d/m/Y");
                        $arrCot[] = "ABO" . $bordereau->getAnneeCotis();
                        $arrCot[] = "LDHINFO ADHERENT";
                        $arrCot[] = $trx->getLdhInfo();
                        $arrCot[] = "1";
                        $arrCot[] = "CH";
                        $arrCot[] = "B-" . $bordereau->getSection()->getCompteComptable() . "-" . $bordereau->getId();
                        $arrCot[] = "N";
                        $arrCot[] = "N";
                        $arrCot[] = "";
                        $arrCot[] = $trx->getNote();

                        fputcsv($handle, array_map(array($this, 'toWin'), $arrCot));
                    }
                }

                // Mettre à jour la date d'exportation
                $bordereau->setDateExport($updatedDate);
            }

            $this->getDoctrine()->getManager()->flush();

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=Windows-1252');
        $response->headers->set('Content-Disposition', 'attachment; filename="bordereaux_prodon.csv"');

        return $response;
    }
    */

    /**
     * @Route("/bordereaux_prodon/{_format}",
     *     name="admin.exporter.bordereaux",
     *     defaults={"_format"="csv"},
     *     requirements={"_format"="csv|xls|xlsx"},
     * )
     *
     */
    public function exporterBordereauxAction()
    {
        $bordereaux = $this->getDoctrine()
            ->getRepository(Bordereau::class)
            ->findNonExportes('ASC');

        return $this->exportProdon($bordereaux);
    }

    /**
     * @Route("/bordereauxExport",
     *     name="admin.export.bordereaux",
     * )
     *
     */
    public function exportBordereauxAction(Request $request)
    {
        $idsBd = $request->request->get('bd');
        $idsPlvt = $request->request->get('plvt');

        if (!empty($request->request->get('expCompta'))) {
            return $this->redirectToRoute('admin.exporter.selected.bordereaux.compta', array('bd' =>$idsBd));
        } elseif (!empty($request->request->get('expProdon'))) {
            return $this->redirectToRoute('admin.exporter.selected.bordereaux.prodon', array('bd' =>$idsBd));
        } elseif (!empty($request->request->get('expPlvt'))) {
            return $this->redirectToRoute('admin.exporter.selected.bordereaux.plvt', array('plvt' =>$idsPlvt));
        } else {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/bordereaux_selected_prodon",
     *     name="admin.exporter.selected.bordereaux.prodon",
     * )
     *
     */
    public function exporterSelectedBordereauxProdonAction(Request $request)
    {
        $request->setRequestFormat('csv');

        $idsBordereaux = $request->query->get('bd');

        $bordereaux = $this->getDoctrine()
            ->getRepository(Bordereau::class)
            ->findById($idsBordereaux);

        return $this->exportProdon($bordereaux);
    }

    /**
     * @Route("/bordereaux_selected_plvt",
     *     name="admin.exporter.selected.bordereaux.plvt",
     * )
     *
     */
    public function exporterSelectedBordereauxPlvtAction(Request $request)
    {
        $request->setRequestFormat('csv');

        $idsBordereaux = $request->query->get('plvt');
        //dump($idsBordereaux);

        $bordereaux = $this->getDoctrine()
            ->getRepository(Bordereau::class)
            ->findById($idsBordereaux);

        return $this->exportPlvt($bordereaux);
    }

    /**
     * @Route("/bordereaux_selected_compta",
     *     name="admin.exporter.selected.bordereaux.compta",
     * )
     *
     */
    public function exporterSelectedBordereauxComptaAction(Request $request)
    {
        $request->setRequestFormat('csv');

        $idsBordereaux = $request->query->get('bd');

        $bordereaux = $this->getDoctrine()
            ->getRepository(Bordereau::class)
            ->findById($idsBordereaux);

        return $this->exportCompta($bordereaux);
    }

    /**
     * @param $bordereaux
     * @return Response
     */
    private function exportProdon($bordereaux) {
        $updatedDate = new \DateTime();

        $content = $this->renderView('admin/exportProdon.twig', array(
            'bordereaux' => $bordereaux,
        ));

        $content = $this->toWin($content);

        // Mettre à jour la date d'exportation
        foreach ($bordereaux as $bordereau) {
            $bordereau->setDateExportProdon($updatedDate);
        }
        $this->getDoctrine()->getManager()->flush();


        $response = new Response($content);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=Windows-1252');
        $response->headers->set('Content-Disposition', 'attachment; filename="bordereaux_prodon.csv"');

        return $response;
    }

    /**
     * @param $bordereaux
     * @return Response
     */
    private function exportPlvt($bordereaux) {
        $updatedDate = new \DateTime();

        $content = $this->renderView('admin/exportPlvt.twig', array(
            'bordereaux' => $bordereaux,
        ));

        $content = $this->toWin($content);

        // Mettre à jour la date d'exportation
        foreach ($bordereaux as $bordereau) {
            $bordereau->setDateExportPlvt($updatedDate);
        }
        $this->getDoctrine()->getManager()->flush();


        $response = new Response($content);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=Windows-1252');
        $response->headers->set('Content-Disposition', 'attachment; filename="bordereaux_plvt.csv"');

        return $response;
    }

    /**
     * @param $bordereaux
     * @return Response
     */
    private function exportCompta($bordereaux) {
        $updatedDate = new \DateTime();

        $content = $this->renderView('admin/exportCiel.twig', array(
            'bordereaux' => $bordereaux,
        ));

        $content = $this->toWin($content);

        // Mettre à jour la date d'exportation
        foreach ($bordereaux as $bordereau) {
            $bordereau->setDateExportCompta($updatedDate);
        }
        $this->getDoctrine()->getManager()->flush();


        $response = new Response($content);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=Windows-1252');
        $response->headers->set('Content-Disposition', 'attachment; filename="bordereaux_ciel.txt"');

        return $response;
    }

    /**
     * @param $str
     * @return null|string|string[]
     */
    private function toWin($str)
    {
        return mb_convert_encoding($str, 'Windows-1252', 'UTF-8');
    }

    /**
     * @Route("/importerFichier",
     *     name="admin.importer.fichier",
     *     )
     * @param Request $request
     * @return Response
     */
    public function importerFichierAction(Request $request)
    {

        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
            ->add('fichier', FileType::class, array(
                'constraints' => array(
                    new NotBlank(),
                )
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            //dump($data);
            /**
             * @var UploadedFile $fichier
             */
            $fichier = $data['fichier'];
            //dump($fichier);

            if ($fichier instanceof UploadedFile) {
                //dump('OK');
                $fichier->move(
                    $this->getParameter('upload_directory'),
                    'Export_sections_pour_bordereau_en_ligne.txt'
                );
            }
            return $this->redirectToRoute('admin.update.sections');
        }

        // ... render the form
        return $this->render('admin/uploadFile.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/importerProfessions",
     *     name="admin.importer.professions",
     *     )
     * @param Request $request
     * @return Response
     */
    public function importerProfessionsAction(Request $request)
    {

        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
            ->add('fichier', FileType::class, array(
                'constraints' => array(
                    new NotBlank(),
                )
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            //dump($data);
            /**
             * @var UploadedFile $fichier
             */
            $fichier = $data['fichier'];
            //dump($fichier);

            if ($fichier instanceof UploadedFile) {
                //dump('OK');
                $fichier->move(
                    $this->getParameter('upload_directory'),
                    'professions.txt'
                );
            }
            return $this->redirectToRoute('admin.update.professions');
        }

        // ... render the form
        return $this->render('admin/uploadProfessions.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/updateSections",
     *     name="admin.update.sections",
     *     )
     * @param UpdateSections $updater
     * @return Response
     */
    public function updateSectionsAction(UpdateSections $updater)
    {

        $retour = $updater->update();

        // ... render the form
        return $this->render('admin/sectionsUpdated.html.twig', array(
            'data' => $retour,
        ));
    }

    /**
     * @Route("/updateProfessions",
     *     name="admin.update.professions",
     *     )
     * @param UpdateProfessions $updater
     * @return Response
     */
    public function updateProfessionsAction(UpdateProfessions $updater)
    {

        $retour = $updater->update();

        // ... render the form
        return $this->render('admin/professionsUpdated.html.twig', array(
            'data' => $retour,
        ));
    }
}
