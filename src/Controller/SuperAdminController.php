<?php

namespace App\Controller;

use App\Entity\Transaction;
use App\Form\TransactionType;
use App\Entity\Section;
use App\Form\SectionType;
use App\Service\Notificator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Bordereau;

/**
 * Admin controller.
 *
 * @Route("superadmin")
 * @IsGranted("ROLE_SUPER_ADMIN")
 */
class SuperAdminController extends Controller
{
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/addUser", name="sa.add.user")
     * @param Request $request
     * @param Notificator $notificator
     * @return Response
     */
    public function addUserAction(Request $request, Notificator $notificator)
    {

        $user1 = new Section();

        $form = $this->createForm(SectionType::class, $user1);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user1->setPlainPassword(uniqid());
            $user1->addRole('ROLE_ADMIN');
            $user1->setEnabled(true);
            $email['sujet'] = 'Votre compte pour les bordereaux en ligne';
            $email['username'] = $user1->getUsername();
            $email['prenom'] = $user1->getNom();
            $notificator->notifyByEmail('nouveauCompteAdmin', $email, $user1->getEmail());

            $this->em->persist($user1);
            $this->em->flush();

            $this->addFlash('success', 'Compte créé');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('superadmin/addUser.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editUser/{id}", name="sa.edit.user",
     *     requirements={
     *         "id"="\d+",
     *     })
     * @param Section $user1
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function editUserAction(Section $user1, Request $request, UserPasswordEncoderInterface $encoder)
    {

        $form = $this->createForm(SectionType::class, $user1);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user1->setPassword($encoder->encodePassword($user1, $user1->getPlainPassword()));
            $this->em->flush();
            $this->addFlash('success', 'Utilisateur modifié');

            return $this->redirectToRoute('sa.user.list');
        }

        return $this->render('superadmin/editUser.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/usersList", name="sa.user.list")
     * @return Response
     */
    public function usersListAction()
    {
        $userManager = $this->get('fos_user.user_manager');
        $users = $userManager->findUsers();

        return $this->render('superadmin/usersList.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * @Route(
     *     "/user/{id}/{type}",
     *     name="sa.user.enable",
     *     requirements={
     *         "id"="\d+",
     *         "type": "on|off",
     *     }
     * )
     * @param $id
     * @param $type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enableUserAction($id, $type)
    {
        $user = $this->getDoctrine()
            ->getRepository(Section::class)
            ->find($id);

        $userManager = $this->get('fos_user.user_manager');
        if ($type == "on" ) {
            $user->setEnabled(true);
            $this->addFlash("success", "L'utilisateur a bien été activé.");
        } else {
            $user->setEnabled(false);
            $this->addFlash("success", "L'utilisateur a bien été désactivé.");
        }
        $userManager->updateUser($user);

        return $this->redirectToRoute('sa.user.list');
    }

    /**
     * @Route(
     *     "/bordereau/{id}/delete",
     *     name="sa.bordereau.delete",
     *     requirements={
     *         "id"="\d+",
     *     },
     *     methods="DELETE"
     * )
     * @param Bordereau $bordereau
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteBordereau(Bordereau $bordereau, Request $request)
    {

        if ($this->isCsrfTokenValid('delete' . $bordereau->getId(), $request->get('_token'))) {
            $transactions = $bordereau->getTransactions();
            foreach ($transactions as $transaction) {
                $this->em->remove($transaction);
            }

            $this->em->remove($bordereau);
            $this->em->flush();
            $this->addFlash('success', 'Bordereau supprimé avec succès');
        }

        return $this->redirectToRoute('admin.bordereaux');
    }

    /**
     * @Route(
     *     "/transaction/{id}/delete",
     *     name="sa.transaction.delete",
     *     requirements={
     *         "id"="\d+",
     *     },
     *     methods="DELETE"
     * )
     * @param Transaction $transaction
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteTransaction(Transaction $transaction, Request $request)
    {

        if ($this->isCsrfTokenValid('delete' . $transaction->getId(), $request->get('_token'))) {
            $bordereau_id = $transaction->getBordereau()->getId();

            $this->em->remove($transaction);
            $this->em->flush();
            $this->addFlash('success', 'Transaction supprimée avec succès');
        }

        return $this->redirectToRoute('voirBordereau', array(
                'index' => $bordereau_id
            ));
    }

    /**
     * @Route(
     *     "/editTransaction/{index}",
     *     name="sa.transaction.edit"
     * )
     * @param $index
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editTransactionAction($index, Request $request, EntityManagerInterface $entityManager)
    {

        /** @var Transaction $trx */
        $trx = $entityManager->getRepository(Transaction::class)->find($index);
        $bordereau_id = $trx->getBordereau()->getId();

        $editTransaction = $trx;

        if (null === $editTransaction) {
            throw $this->createNotFoundException();
        }


        $form = $this->createForm(TransactionType::class, $editTransaction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //Enregistrement de modifications
            $entityManager->flush();

            $this->addFlash('success', 'Ligne modifiée');

            return $this->redirectToRoute('voirBordereau', array(
                'index' => $bordereau_id
            ));
        }
        return $this->render('bordereau/editTransaction.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
