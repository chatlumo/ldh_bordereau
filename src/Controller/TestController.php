<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Notificator;

/**
 * Test controller.
 *
 * @Route("test")
 * @IsGranted("ROLE_SUPER_ADMIN")
 */
class TestController extends Controller
{
    protected $notificator;

    public function __construct(Notificator $notificator)
    {
        $this->notificator = $notificator;
    }

    /**
     * @Route("/email", name="email")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function email(Request $request) {

        $datas = array(
            'sujet'     => 'Nouveau bordereau envoyé',
            'section'   => 'TEST',
        );
        $this->notificator->notifyByEmail('nouveauBordereau', $datas);

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }
}
