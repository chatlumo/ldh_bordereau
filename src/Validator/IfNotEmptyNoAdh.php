<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IfNotEmptyNoAdh extends Constraint
{
    public $message = "Vous avez renseigné le N° d'adhérent ce qui doit nous permettre d'éviter la création d'un doublon dans notre système.
    Cependant, vous devez aussi remplir au minimum : le sexe, le nom et le prénom.";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
