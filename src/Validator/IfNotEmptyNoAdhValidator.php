<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IfNotEmptyNoAdhValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {

        $trx = $protocol;

        if ( !empty($trx->getNoAdh()) ) {
            if (empty($trx->getCivilite()) || empty($trx->getNom()) || empty($trx->getPrenom())) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}