<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class VerifCotisation extends Constraint
{
    public $message = "Vous devez saisir au moins une cotisation, ou un abonnement ou un don.
    Si vous saisissez une cotisation, celle doit être au minimum de 10 €.";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
