<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsAddressOk extends Constraint
{
    public $message = "Vous ne devez remplir aucun champ d'adresse (par exemple si c'est un renouvellement sans changement). Si vous remplissez un champ d'adresse, vous devez tous les remplir (adresse, code postal et ville).";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
