<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IfEmptyNoAdh extends Constraint
{
    public $message = "Vous n'avez pas indiqué le numéro d'adhérent. Nous considérons que l'adhérent est peut-être nouveau. Aussi, vous devez renseigner le sexe, nom, prénom, adresse, code postal et ville. 
En fournissant le n° d'adhérent, seul le sexe, le nom et le prénom sont obligatoires (les autres informations étant déjà connus du siège s'il n'y a pas de changement).";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
