<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IfEmptyNoAdhValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {

        $trx = $protocol;

        if ( empty($trx->getNoAdh()) ) {
            if ( empty($trx->getCivilite()) || empty($trx->getNom()) || empty($trx->getPrenom()) || empty($trx->getCodePostal()) || empty($trx->getAdresse()) || empty($trx->getVille()) ) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}