<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class VerifCotisationValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {

        $trx = $protocol;

        // Si aucun paiement n'est saisi
        if (
            empty($trx->getCotisation())
            && empty($trx->getHl())
            && empty($trx->getLdhInfo())
            && empty($trx->getDonSection())
            && empty($trx->getDonSiege())
        )
        {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }

        // Si la cotisation est saisie mais inférieure à 10 €
        if ( !empty($trx->getCotisation()) ) {
            if ($trx->getCotisation() < 10) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }
    }
}