<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsAddressOkValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {

        $trx = $protocol;

        if (empty($trx->getCodePostal()) && empty($trx->getAdresse()) && empty($trx->getVille())) {
            // All address fields are empty
        }
        elseif ( !(!empty($trx->getCodePostal()) && !empty($trx->getAdresse()) && !empty($trx->getVille())) ) {
            // One or more address field is empty but at least one is not empty
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}