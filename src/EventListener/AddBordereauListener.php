<?php

namespace App\EventListener;

use App\Event\AddBordereauEvent;
use App\Service\Notificator;

class AddBordereauListener
{
    protected $notificator;

    public function __construct(Notificator $notificator)
    {
        $this->notificator = $notificator;
    }

    public function notify(AddBordereauEvent $event) {
        $datas = array(
            'sujet'     => 'Nouveau bordereau envoyé',
            'section'   => $event->getBordereau()->getSection()->getNom(),
        );
        $this->notificator->notifyByEmail('nouveauBordereau', $datas);
    }

}