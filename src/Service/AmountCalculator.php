<?php

namespace App\Service;

use App\Entity\Bordereau;

class AmountCalculator
{
    const COEFF_PART_SIEGE = 2;
    const COEFF_PART_SECTION = 1;
    const TVA = 0.021;

    /**
     * @param Bordereau $bordereau
     * @return int
     */
    public function partSiege(Bordereau $bordereau) {


        $transactions = $bordereau->getTransactions();

        /** @var int $total */
        $total = 0;

        foreach ($transactions as $trx) {
            $cotisation = round($trx->getCotisation() * self::COEFF_PART_SIEGE / (self::COEFF_PART_SIEGE + self::COEFF_PART_SECTION), 2);
            $donSiege = $trx->getDonSiege();
            $hl = $trx->getHl();
            $ldhInfo =$trx->getLdhInfo();

            $total += $cotisation + $donSiege + $hl + $ldhInfo;
        }

        return $total;

    }

    /**
     * @param Bordereau $bordereau
     * @return int
     */
    public function partSection(Bordereau $bordereau) {


        $transactions = $bordereau->getTransactions();

        /** @var int $total */
        $total = 0;

        foreach ($transactions as $trx) {
            $cotisation = round($trx->getCotisation() * self::COEFF_PART_SECTION / (self::COEFF_PART_SIEGE + self::COEFF_PART_SECTION), 2);

            $total += $cotisation;
        }

        return $total;

    }

    /**
     * @param Bordereau $bordereau
     * @return int
     */
    public function totalCotisation(Bordereau $bordereau) {


        $transactions = $bordereau->getTransactions();

        /** @var int $total */
        $total = 0;

        foreach ($transactions as $trx) {
            $cotisation = round($trx->getCotisation(), 2);

            $total += $cotisation;
        }

        return $total;

    }

    /**
     * @param Bordereau $bordereau
     * @return int
     */
    public function totalDonSiege(Bordereau $bordereau) {


        $transactions = $bordereau->getTransactions();

        /** @var int $total */
        $total = 0;

        foreach ($transactions as $trx) {
            $donSiege = round($trx->getDonSiege(), 2);

            $total += $donSiege;
        }

        return $total;

    }

    /**
     * @param Bordereau $bordereau
     * @return int
     */
    public function totalLdhInfo(Bordereau $bordereau) {


        $transactions = $bordereau->getTransactions();

        /** @var int $total */
        $total = 0;

        foreach ($transactions as $trx) {
            $ldhInfo = round($trx->getLdhInfo(), 2);

            $total += $ldhInfo;
        }

        return $total;

    }

    private function totalHlTTC(Bordereau $bordereau)
    {
        $transactions = $bordereau->getTransactions();

        /** @var int $total */
        $total = 0;

        foreach ($transactions as $trx) {
            $hl = round($trx->getHl(), 2);

            $total += $hl;
        }

        return $total;

    }

    public function totalHlHT(Bordereau $bordereau) {
        return round($this->totalHlTTC($bordereau) * (1 - self::TVA), 2);
    }

    public function totalHlTVA(Bordereau $bordereau) {
        return round($this->totalHlTTC($bordereau) * self::TVA, 2);
    }

}