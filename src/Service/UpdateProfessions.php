<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Profession;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Repository\professionRepository;

class UpdateProfessions
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var professionRepository
     */
    private $professionRepository;

    /**
     * UpdateProfessions constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->professionRepository = $entityManager->getRepository(Profession::class);
    }

    /**
     * @return int|null|void
     */
    public function update()
    {
        ini_set("memory_limit", "-1");

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder($delimiter = "\t", $enclosure = '"')]);

        $csvFile = __DIR__ . '/../../uploads/' . 'professions.txt';
        $csvContents=file_get_contents($csvFile);
        //$csvConverted = mb_convert_encoding($csvContents, "UTF-8", "Windows-1252");

        // decoding CSV contents
        $datas = $serializer->decode($csvContents, 'csv');

        // On supprime toutes les professions
        $this->professionRepository->deleteAll();


        foreach($datas as $data) {
            $profession = new Profession();
            $profession->setLibelle(trim($data['libelle']));

            $this->entityManager->persist($profession);

        }

        // On sauvegarde les modifications
        $this->entityManager->flush();

        return "Fichier Importé !";

    }
}