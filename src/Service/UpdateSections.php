<?php
/*
 * Version pour des comptes de trésoriers
 * OBSOLETE
 */


namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Section;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Repository\SectionRepository;

class UpdateSections
{
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SectionRepository
     */
    private $sectionRepository;


    /**
     * UpdateUsersAndSections constructor.
     * @param Notificator $notificator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Notificator $notificator, EntityManagerInterface $entityManager)
    {
        $this->notificator = $notificator;
        $this->entityManager = $entityManager;
        $this->sectionRepository = $entityManager->getRepository(Section::class);
    }

    /**
     * @return int|null|void
     */
    public function update()
    {
        ini_set("memory_limit", "-1");

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder($delimiter = "\t", $enclosure = '"')]);

        $csvFile = __DIR__ . '/../../uploads/' . 'Export_sections_pour_bordereau_en_ligne.txt';
        $csvConverted=file_get_contents($csvFile);

        // decoding CSV contents
        $datas = $serializer->decode($csvConverted, 'csv');


        //dump($sections);

        // On désactive toutes les sections
        $this->sectionRepository->disableSections();

        // Initialisation du mail
        $email['sujet'] = "Création de votre compte pour l'ajout de bordereaux en ligne";


        foreach($datas as $data) {
            $idSection = $this->formatIdClient($data['No client']);

            /**
             * Récupération de la section dans la base si elle existe.
             * @var $section Section
             */
            $section = $this->sectionRepository->find($idSection);


            if (null === $section) {
                $section = new Section();
                $section->setId($idSection);
                $section->setPlainPassword(uniqid());
                $email['prenom'] = $data['Nom/Compagnie'];
                $email['noAdh'] = $data['No client'];
                $email['section'] = $data['Nom/Compagnie'];
                $this->notificator->notifyByEmail('nouveauCompte', $email, $data['Courriel préféré'], true);
            }
            $section->setNom($data['Nom/Compagnie']);
            $section->setUsername($this->formatIdClient($data['No client']));
            $section->setCompteComptable($data['Compte section']);
            $section->setIban($data['IBAN']);
            $section->setBic($data['BIC']);
            $section->setRum($data['RUM Structure']);
            $section->setEmail($data['Courriel préféré']);
            $section->setEnabled(true);
            $section->addRole('ROLE_TRESORIER');


            $this->entityManager->persist($section);

        }
        //On active la section de test
        $sectionTest = $this->sectionRepository->find(999998);
        if (null === $sectionTest) {
            $sectionTest = new Section();
            $sectionTest->setId($this->formatIdClient(999998));
            $sectionTest->setPlainPassword(uniqid());
            $email['prenom'] = 'Julien';
            $email['noAdh'] = 999998;
            $email['section'] = 'SECTION DE TEST';
            $sectionTest->setNom('SECTION DE TEST');
            $sectionTest->setUsername(999998);
            $sectionTest->setCompteComptable(999998);
            $sectionTest->setEmail('julien.jabouin@ldh-france.org');
            $this->notificator->notifyByEmail('nouveauCompte', $email, 'julien.jabouin@ldh-france.org', true);
            $sectionTest->addRole('ROLE_TRESORIER');
        }
        $sectionTest->setEnabled(true);
        $this->entityManager->persist($sectionTest);

        //On active la section de test
        $sectionSA = $this->sectionRepository->find(999999);
        if (null === $sectionSA) {
            $sectionSA = new Section();
            $sectionSA->setId($this->formatIdClient(999999));
            $sectionSA->setPlainPassword(uniqid());
            $email['prenom'] = 'Julien';
            $email['noAdh'] = 999999;
            $email['section'] = 'JULIEN JABOUIN';
            $sectionSA->setNom('JULIEN JABOUIN');
            $sectionSA->setUsername('jjabouin');
            $sectionSA->setCompteComptable(999999);
            $sectionSA->setEmail('jjabouin@gmail.com');
            $this->notificator->notifyByEmail('nouveauCompte', $email, 'jjabouin@gmail.com', true);
            $sectionSA->addRole('ROLE_SUPER_ADMIN');
        }
        $sectionSA->setEnabled(true);
        $this->entityManager->persist($sectionSA);

        // On sauvegarde les modifications
        $this->entityManager->flush();

        return "Fichier Importé !";

    }

    private function formatIdClient($idClient) {
        $id = str_replace(array(chr(194), chr(160), chr(32)), '', $idClient);
        return (int) $id;
    }
}