<?php
namespace App\Service;

use App\Entity\Section;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Doctrine\ORM\EntityManagerInterface;


class ImportSection
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function import() {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder($delimiter = "\t", $enclosure = '"')]);

        /*
        $birdRepository = $this->em->getRepository('App:Bird');
        $nb = $birdRepository->count();
        dump($nb);

        if ($nb > 0) {
            throw new \Exception('La table des références TaxRef n\'est pas vide !');
        }
        */

        $csvFile = __DIR__ . '/../../../var/' . 'Export_tresoriers_pour_bordereau_en_ligne.txt';
        $csvContents=file_get_contents($csvFile);
        $csvConverted = mb_convert_encoding($csvContents, "UTF-8", "Windows-1252");

        // decoding CSV contents
        $datas = $serializer->decode($csvConverted, 'csv');

        foreach($datas as $data) {
            $section = new Section();
            $section->setId($this->formatIdClient($data['C1-No client']));
            $section->setNom($data['C1-Nom/Compagnie']);
            $section->setCompteComptable($data['C1-Compte section']);
            //dump($data);
            //dump($this->formatIdClient($data['C1-No client']));
            //dump($this->getFonction($data['R_Relation']));


            $this->em->persist($section);
        }
        $this->em->flush();

        return "Fichier importé !";
    }

    private function formatIdClient($idClient) {
        $id = str_replace(' ', '', $idClient);
        return (int) $id;
    }

    private function getFonction($fonction) {
        if ( strstr($fonction, 'Trésorier') ) {
          $fonction = 'Trésorier';
        }
        return $fonction;
    }
}
