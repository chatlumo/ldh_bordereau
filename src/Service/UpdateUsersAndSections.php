<?php
/*
 * Version pour des comptes de trésoriers
 * OBSOLETE
 */


namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Section;
use App\Entity\User;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Repository\SectionRepository;
use App\Repository\UserRepository;

class UpdateUsersAndSections
{
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SectionRepository
     */
    private $sectionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UpdateUsersAndSections constructor.
     * @param Notificator $notificator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(Notificator $notificator, EntityManagerInterface $entityManager)
    {
        $this->notificator = $notificator;
        $this->entityManager = $entityManager;
        $this->sectionRepository = $entityManager->getRepository(Section::class);
        $this->userRepository = $entityManager->getRepository(User::class);
    }

    /**
     * @return int|null|void
     */
    public function update()
    {
        ini_set("memory_limit", "-1");

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder($delimiter = "\t", $enclosure = '"')]);

        $csvFile = __DIR__ . '/../../uploads/' . 'Export_tresoriers_pour_bordereau_en_ligne.txt';
        $csvConverted=file_get_contents($csvFile);
        //$csvConverted = mb_convert_encoding($csvContents, "UTF-8", "Windows-1252");

        // decoding CSV contents
        $datas = $serializer->decode($csvConverted, 'csv');


        //dump($sections);

        // On désactive toutes les sections
        $this->sectionRepository->disableSections();

        // On désactive les utilisateurs avec le rôle trésorier
        $this->userRepository->disableTresoriers();

        // Initialisation du mail
        $email['sujet'] = "Création de votre compte pour l'ajout de bordereaux en ligne";


        foreach($datas as $data) {
            $idSection = $this->formatIdClient($data['C1-No client']);

            /**
             * Récupération de la section dans la base si elle existe.
             * @var $section Section
             */
            $section = $this->sectionRepository->find($idSection);


            if (null === $section) {
                $section = new Section();
                $section->setId($this->formatIdClient($data['C1-No client']));
            }
            $section->setNom($data['C1-Nom/Compagnie']);
            $section->setCompteComptable($data['C1-Compte section']);
            $section->setIban($data['C1-IBAN']);
            $section->setBic($data['C1-BIC']);
            $section->setRum($data['C1-RUM Structure']);
            $section->setEnabled(true);


            // On s'occupe du trésorier
            $noAdh = $this->formatIdClient($data['C2-No client']);

            /**
             * Récupération du trésorier dans la base si il existe.
             * @var $tresorier User
             */
            $tresorier = $this->userRepository->findOneBy(array('noAdh' => $noAdh));

            if (null === $tresorier) {
                $tresorier = new User();
                $tresorier->setNoAdh($noAdh);
                $tresorier->setPlainPassword(uniqid());
                // Envoi d'un email pour la création du compte
                $email['prenom'] = $data['C2-Prénom/Service'];
                $email['noAdh'] = $noAdh;
                $email['section'] = $data['C1-Nom/Compagnie'];
                $this->notificator->notifyByEmail('nouveauCompte', $email, $data['C2-Courriel préféré'], true);
            }
            $tresorier->setNom($data['C2-Nom/Compagnie']);
            $tresorier->setPrenom($data['C2-Prénom/Service']);
            $tresorier->setEmail($data['C2-Courriel préféré']);
            $tresorier->setUsername($noAdh);
            $tresorier->setEnabled(true);
            $tresorier->setFonction('Trésorier');
            $tresorier->addRole('ROLE_TRESORIER');
            $tresorier->setSectionTresorier($section);

            $this->entityManager->persist($section);
            $this->entityManager->persist($tresorier);

        }
        //On active le trésorier de test
        $tresorier = $this->userRepository->findOneBy(array('noAdh' => 999998));
        $tresorier->setEnabled(true);
        $this->entityManager->persist($tresorier);

        // On sauvegarde les modifications
        $this->entityManager->flush();

        return "Fichier Importé !";

    }

    private function formatIdClient($idClient) {
        $id = str_replace(' ', '', $idClient);
        return (int) $id;
    }

    /*
    private function getFonction($fonction) {
        if ( strstr($fonction, 'Trésorier') ) {
          $fonction = 'Trésorier';
        }
        return $fonction;
    }
    */
}