<?php

namespace App\Service;


class Notificator
{
    protected $twig;
    protected $mailer;
    protected $mailFrom;

    public function __construct(\Twig_Environment $twig_Environment, \Swift_Mailer $mailer, $mailFrom)
    {
        $this->twig = $twig_Environment;
        $this->mailer = $mailer;
        $this->mailFrom = $mailFrom;
    }

    // Méthode pour notifier par e-mail
    public function notifyByEmail($template, $datas, $setTo = null, $setBcc = null)
    {
        $mailTo = $setTo;

        if (null === $mailTo) {
            $mailTo = $this->mailFrom;
        }

        $message = (new \Swift_Message())
            ->setSubject("[LDH] ".$datas['sujet'])
            ->setFrom($this->mailFrom)
            ->setTo($mailTo)
            ->setBody($this->twig->render('email/'.$template.'.html.twig', array(
                'datas' => $datas
            )))
            ->setContentType("text/html")
        ;

        if ($setBcc) {
            if ($setBcc === true) {
                $message->setBcc($this->mailFrom);
            } else {
                $message->setBcc($setBcc);
            }
        }

        $this->mailer->send($message);
    }
}