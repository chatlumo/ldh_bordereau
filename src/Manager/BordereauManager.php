<?php

namespace App\Manager;

use App\Entity\Bordereau;
use App\Entity\Section;
use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
//use App\Exception\BordereauManagerException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Class BordereauManager
 * @package App\Manager
 */
class BordereauManager
{
    private $session;
    private $em;
    private $validator;
    private $emailSender;
    private $translator;


    /**
     * BordereauManager constructor.
     * @param SessionInterface $session
     * @param EntityManagerInterface $em
     * @param ValidatorInterface $validator
     * @param TranslatorInterface $translator
     * @param EmailSender $emailSender
     */
    public function __construct(SessionInterface $session, EntityManagerInterface $em, ValidatorInterface $validator) {
        $this->session = $session;
        $this->em = $em;
        $this->validator = $validator;
        /*
        $this->translator = $translator;
        $this->emailSender = $emailSender;
        */
    }

    /**
     * @return Bordereau
     */
    public function initBordereau() {

        //$this->session->clear();

        $bordereau = $this->getSessionBordereau();


        if ( !$bordereau ) {
            $bordereau = new Bordereau();
            $this->setSessionBordereau($bordereau);
        }

        return $bordereau;

    }

    /**
     * @return Bordereau|null
     */
    public function getBordereau() {

        $bordereau = $this->getSessionBordereau();

        if ( !$bordereau ) {
            //throw new BordereauManagerException('app.fill.form1.first');
        }

        return $bordereau;

        //return $this->bordereau;
    }

    /**
     * @return Bordereau|null
     */
    public function getBordereauFromTransaction(Transaction $trx) {

        $bordereau = $trx->getBordereau();

        return $bordereau;
    }

    /**
     * @param Bordereau $bordereau
     */
    public function setBordereau(Bordereau $bordereau) {
        $this->setSessionBordereau($bordereau);
    }

    /**
     * @param Bordereau $bordereau
     */
    /*
    public function saveBordereau(Bordereau $bordereau) {
        //Creating empty transactions while number of transactions is less than number of adh chosen

        while (count($bordereau->getTransactions()) < $bordereau->getNbAdh()) {
            $transaction = new Transaction();
            $bordereau->addTransaction($transaction);
        }

        // Delete last X transactions if there are too much (user selects X fewer adh)
        while (count($bordereau->getTransactions()) > $bordereau->getNbAdh()) {
            $transaction = $bordereau->getTransactions()->last();
            $bordereau->removeTransaction($transaction);
        }


        $this->setSessionBordereau($bordereau);
    }
    */

    /**
     * @param Bordereau $bordereau
     * @param Section $user
     * @return void
     */
    public function saveBordereau(Bordereau $bordereau, Section $user) {


        /*
        $errors = $this->validator->validate($bordereau);

        if (count($errors) > 0) {
            $errMsg = '';
            foreach ($errors as $error) {
                $errMsg .= $error->getMessage().' ';
            }

            throw new BordereauManagerException($errMsg);
        }
        */


        $bordereau->setSection($user);
        $this->em->persist($bordereau);
        $this->em->flush();

        //$bordereauRef = 'B-' . $bordereau->getSection()->getCompteComptable() . '-' . $bordereau->getId();

        /*$bordereauSaved = $bordereau;

        $bordereau = null;

        */
        $this->session->remove('bordereau');

        return $bordereau;


        //send email to customer
        /*
        $subject = $this->translator->trans('app.email.subject');
        $this->emailSender->sendEmail( $subject, $bordereau);
        */
    }

    /**
     * @return mixed|null
     */
    private function getSessionBordereau() {
        if ($this->session->has('bordereau')) {
            if ($this->session->get('bordereau') instanceof Bordereau) {
                return $this->session->get('bordereau');
            }
        }

        return null;
    }

    /**
     * @param Bordereau $bordereau
     */
    private function setSessionBordereau(Bordereau $bordereau) {
        $this->session->set('bordereau', $bordereau);

    }

    /*
    public function addTransaction(Bordereau $bordereau, Transaction $transaction) {
        $bordereau->addTransaction($transaction);
        $this->em->persist($bordereau);
        $this->em->flush();
    }
    */

}