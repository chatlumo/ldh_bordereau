<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as CustomAssert;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 *
 * @CustomAssert\VerifCotisation()
 */
class Transaction
{
    const TARIF_HL = 20;
    const TARIF_LDHINFO = 10;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="cotisation", type="float", nullable=true)
     * @Assert\Range(
     *     min = 10,
     *     minMessage="La cotisation ne peut être inférieure à {{ limit }} €."
     * )
     */
    private $cotisation;

    /**
     * @var integer
     *
     * @ORM\Column(name="hl", type="integer", nullable=true)
     */
    private $hl = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="ldh_info", type="integer", nullable=true)
     */
    private $ldhInfo = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="don_siege", type="float", nullable=true)
     */
    private $donSiege;

    /**
     * @var float
     *
     * @ORM\Column(name="don_section", type="float", nullable=true)
     */
    private $donSection;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true)
     */
    private $total;

    /**
     * @var Bordereau
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Bordereau", inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bordereau;

    /**
     * @var Adherent
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Adherent", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $adherent;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="date")
     */
    private $paiementDate;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $paiementType;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $paiementRef;

    public function __construct()
    {

    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bordereau
     *
     * @param \App\Entity\Bordereau $bordereau
     *
     * @return Transaction
     */
    public function setBordereau(\App\Entity\Bordereau $bordereau)
    {
        $this->bordereau = $bordereau;

        return $this;
    }

    /**
     * Get bordereau
     *
     * @return \App\Entity\Bordereau
     */
    public function getBordereau()
    {
        return $this->bordereau;
    }

    /**
     * Set adherent
     *
     * @param \App\Entity\Adherent $adherent
     *
     * @return Transaction
     */
    public function setAdherent(\App\Entity\Adherent $adherent)
    {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * Get adherent
     *
     * @return \App\Entity\Adherent
     */
    public function getAdherent()
    {
        return $this->adherent;
    }

    /**
     * Set cotisation
     *
     * @param float $cotisation
     *
     * @return Transaction
     */
    public function setCotisation($cotisation)
    {
        $this->cotisation = $cotisation;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return Transaction
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get cotisation
     *
     * @return float
     */
    public function getCotisation()
    {
        return $this->cotisation;
    }

    /**
     * Set hl
     *
     * @param integer $hl
     *
     * @return Transaction
     */
    public function setHl($hl)
    {
        $this->hl = $hl;

        return $this;
    }

    /**
     * @return bool
     */
    public function getHlSubscription()
    {
        return (bool) $this->hl;
    }

    /**
     * @param bool $bool
     */
    public function setHlSubscription($bool)
    {
        $this->hl = $bool ? self::TARIF_HL : 0;
    }

    /**
     * @return bool
     */
    public function getLdhInfoSubscription()
    {
        return (bool) $this->ldhInfo;
    }

    /**
     * @param bool $bool
     */
    public function setLdhInfoSubscription($bool)
    {
        $this->ldhInfo = $bool ? self::TARIF_LDHINFO : 0;
    }

    /**
     * Get hl
     *
     * @return integer
     */
    public function getHl()
    {
        return $this->hl;
    }

    /**
     * Set ldhInfo
     *
     * @param integer $ldhInfo
     *
     * @return Transaction
     */
    public function setLdhInfo($ldhInfo)
    {
        $this->ldhInfo = $ldhInfo;

        return $this;
    }

    /**
     * Get ldhInfo
     *
     * @return integer
     */
    public function getLdhInfo()
    {
        return $this->ldhInfo;
    }

    /**
     * Set donSiege
     *
     * @param float $donSiege
     *
     * @return Transaction
     */
    public function setDonSiege($donSiege)
    {
        $this->donSiege = $donSiege;

        return $this;
    }

    /**
     * Get donSiege
     *
     * @return float
     */
    public function getDonSiege()
    {
        return $this->donSiege;
    }

    /**
     * Set donSection
     *
     * @param float $donSection
     *
     * @return Transaction
     */
    public function setDonSection($donSection)
    {
        $this->donSection = $donSection;

        return $this;
    }

    /**
     * Get donSection
     *
     * @return float
     */
    public function getDonSection()
    {
        return $this->donSection;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Transaction
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    public function getPaiementDate(): ?\DateTimeInterface
    {
        return $this->paiementDate;
    }

    public function setPaiementDate(\DateTimeInterface $paiementDate): self
    {
        $this->paiementDate = $paiementDate;

        return $this;
    }

    public function getPaiementType(): ?string
    {
        return $this->paiementType;
    }

    public function setPaiementType(string $paiementType): self
    {
        $this->paiementType = $paiementType;

        return $this;
    }

    public function getPaiementRef(): ?string
    {
        return $this->paiementRef;
    }

    public function setPaiementRef(?string $paiementRef): self
    {
        $this->paiementRef = $paiementRef;

        return $this;
    }


}
