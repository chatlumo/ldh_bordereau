<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Section
 *
 * @ORM\Table(name="section")
 * @ORM\Entity(repositoryClass="App\Repository\SectionRepository")
 * @UniqueEntity("id")
 * @UniqueEntity("compteComptable")
 */
class Section extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="compte_comptable", type="integer", unique=true)
     */
    private $compteComptable;


    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=27, nullable=true)
     */
    private $iban;

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=11, nullable=true)
     */
    private $bic;

    /**
     * @var string
     *
     * @ORM\Column(name="rum", type="string", length=35, nullable=true)
     */
    private $rum;


    public function __construct()
    {
        parent::__construct();
        $this->setEnabled(true);
    }

    public function __toString()
    {
        return $this->nom;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Section
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }



    /**
     * Set compteComptable
     *
     * @param integer $compteComptable
     *
     * @return Section
     */
    public function setCompteComptable($compteComptable)
    {
        $this->compteComptable = $compteComptable;

        return $this;
    }

    /**
     * Get compteComptable
     *
     * @return integer
     */
    public function getCompteComptable()
    {
        return $this->compteComptable;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Section
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getBic(): ?string
    {
        return $this->bic;
    }

    public function setBic(?string $bic): self
    {
        $this->bic = $bic;

        return $this;
    }

    public function getRum(): ?string
    {
        return $this->rum;
    }

    public function setRum(?string $rum): self
    {
        $this->rum = $rum;

        return $this;
    }

    public function isPlvt(): ?bool
    {
        $plvt = false;

        if (!empty($this->iban) && !empty($this->bic) && !empty($this->rum)) {
            $plvt =true;
        }

        return $plvt;
    }
}
