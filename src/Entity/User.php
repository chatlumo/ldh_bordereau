<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 20/03/2018
 * Time: 20:55
 */

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user", indexes={@Index(name="search_id_prodon", columns={"noAdh"})})
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="noAdh", type="integer", nullable=true)
     */
    private $noAdh;

    /**
     * @return int
     */
    public function getNoAdh()
    {
        return $this->noAdh;
    }

    /**
     * @param int $noAdh
     */
    public function setNoAdh($noAdh)
    {
        $this->noAdh = $noAdh;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    private $fonction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Section", inversedBy="tresorier")
     */
    private $sectionTresorier;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     *
     * @return User
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set section
     *
     * @param \App\Entity\Section $sectionTresorier
     *
     * @return User
     */
    public function setSectionTresorier(\App\Entity\Section $sectionTresorier)
    {
        $this->sectionTresorier = $sectionTresorier;

        return $this;
    }

    /**
     * Get section
     *
     * @return \App\Entity\Section
     */
    public function getSectionTresorier()
    {
        return $this->sectionTresorier;
    }
}
