<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Bordereau
 *
 * @ORM\Table(name="bordereau")
 * @ORM\Entity(repositoryClass="App\Repository\BordereauRepository")
 */
class Bordereau
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSaisie", type="datetime")
     */
    private $dateSaisie;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExport", type="datetime", nullable=true)
     */
    private $dateExportProdon;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExportCompta", type="datetime", nullable=true)
     */
    private $dateExportCompta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExportPlvt", type="datetime", nullable=true)
     */
    private $dateExportPlvt;

    /**
     * @var bool
     *
     * @ORM\Column(name="paiementPlvt", type="boolean", options={"default":false})
     */
    private $paiementPlvt;

    /**
     * @var int
     *
     * @ORM\Column(name="anneeCotis", type="integer")
     */
    private $anneeCotis;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Section", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $section;

    /**
     * @var Transactions[]
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="bordereau", cascade={"persist"}, fetch="EAGER")
     *
     * @Assert\Valid()
     */
    private $transactions;

    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean", options={"default":false})
     */
    private $validated;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateSaisie
     *
     * @param \DateTime $dateSaisie
     *
     * @return Bordereau
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;

        return $this;
    }

    /**
     * Get dateSaisie
     *
     * @return \DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set anneeCotis
     *
     * @param integer $anneeCotis
     *
     * @return Bordereau
     */
    public function setAnneeCotis($anneeCotis)
    {
        $this->anneeCotis = $anneeCotis;

        return $this;
    }

    /**
     * Get anneeCotis
     *
     * @return int
     */
    public function getAnneeCotis()
    {
        return $this->anneeCotis;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transactions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setDateSaisie(new \DateTime());
        $this->validated = false;
    }

    /**
     * Add transaction
     *
     * @param \App\Entity\Transaction $transaction
     *
     * @return Bordereau
     */
    public function addTransaction(\App\Entity\Transaction $transaction)
    {
        $this->transactions[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \App\Entity\Transaction $transaction
     */
    public function removeTransaction(\App\Entity\Transaction $transaction)
    {
        $this->transactions->removeElement($transaction);
    }

    /**
     * Get transactions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Set section
     *
     * @param \App\Entity\Section $section
     *
     * @return Bordereau
     */
    public function setSection(\App\Entity\Section $section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \App\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Bordereau
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set dateExportProdon
     *
     * @param \DateTime $dateExportProdon
     *
     * @return Bordereau
     */
    public function setDateExport($dateExportProdon)
    {
        $this->dateExportProdon = $dateExportProdon;

        return $this;
    }

    /**
     * Get dateExportProdon
     *
     * @return \DateTime
     */
    public function getDateExportProdon()
    {
        return $this->dateExportProdon;
    }

    /**
     * Set dateExportProdon
     *
     * @param \DateTime $dateExportProdon
     *
     * @return Bordereau
     */
    public function setDateExportProdon($dateExportProdon)
    {
        $this->dateExportProdon = $dateExportProdon;

        return $this;
    }

    /**
     * Set dateExportCompta
     *
     * @param \DateTime $dateExportCompta
     *
     * @return Bordereau
     */
    public function setDateExportCompta($dateExportCompta)
    {
        $this->dateExportCompta = $dateExportCompta;

        return $this;
    }

    /**
     * Get dateExportCompta
     *
     * @return \DateTime
     */
    public function getDateExportCompta()
    {
        return $this->dateExportCompta;
    }

    public function getDateExportPlvt(): ?\DateTimeInterface
    {
        return $this->dateExportPlvt;
    }

    public function setDateExportPlvt(?\DateTimeInterface $dateExportPlvt): self
    {
        $this->dateExportPlvt = $dateExportPlvt;

        return $this;
    }

    public function getPaiementPlvt(): ?bool
    {
        return $this->paiementPlvt;
    }

    public function setPaiementPlvt(bool $paiementPlvt): self
    {
        $this->paiementPlvt = $paiementPlvt;

        return $this;
    }
}
