<?php

namespace App\Event;

use App\Entity\Bordereau;
use Symfony\Component\EventDispatcher\Event;


class AddBordereauEvent extends Event
{
    protected $bordereau;

    public function __construct(Bordereau $bordereau)
    {
        $this->bordereau = $bordereau;
    }

    public function getBordereau() {
        return $this->bordereau;
    }
}
