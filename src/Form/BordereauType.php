<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BordereauType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('anneeCotis', ChoiceType::class, array(
                'choices' => array(
                    date("Y") => date("Y"),
                    date("Y") - 1 => date("Y") - 1,
                    date("Y") + 1 => date("Y") + 1,
                ),
                'label' => 'Année de cotisation',
            ))
            ->add('paiementPlvt', CheckboxType::class, array(
            'label' => "Je choisis que la part siège de ce bordereau soit prélevée sur le compte bancaire de la section, en lieu et place de l'envoi d'un chèque",
            //'value' => Transaction::TARIF_HL,
            'required' => false,
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Bordereau'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bordereau';
    }


}
