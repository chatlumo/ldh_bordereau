<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionsFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transactions', CollectionType::class, array(
                'entry_type' => TransactionType::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'entry_options' => array(
                    'label' => 'Détail des adhérents',
                ),
                'label' => false,
            ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Bordereau',
            'validation_groups' => array('step2'),
        ]);
    }

}