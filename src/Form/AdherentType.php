<?php

namespace App\Form;

use App\Entity\Profession;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class AdherentType extends AbstractType
{
    protected $minYear, $maxYear;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $now = new \DateTime('now');
        $this->maxYear = (int) $now->format('Y');
        $this->minYear = $this->maxYear - 100;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('noAdh', TextType::class, array(
                'label' => 'N° d\'adhérent (que des chiffres, sans espace, sans 0 devant)',
                'required' => false,
                'attr' => array(
                    'pattern' => '^[1-9]{1}[0-9]{0,5}$',
                )
            ))
            ->add('civilite', ChoiceType::class, array(
                'label' => 'Civilité',
                'expanded' => true,
                'choices' => array(
                    'Femme' => 'Madame',
                    'Homme' => 'Monsieur',
                    'Neutre' => 'Mx',
                )
            ))
            ->add('nom', TextType::class, array(
                'label' => 'Nom',
                'attr' => array(
                    'maxlength' => 100,
                )

            ))
            ->add('prenom', TextType::class, array(
                'label' => 'Prénom',
                'attr' => array(
                    'maxlength' => 100,
                )

            ))
            ->add('profession', ChoiceType::class, array(
                'label' => 'Profession ou ancienne profession',
                'choices' => $this->getChoices(),
                'required' => false,

            ))
            ->add('adresse', TextareaType::class, array(
                'required' => false,
                'label' => 'Adresse',
                'attr' => array(
                    'maxlength' => 140,
                )

            ))
            ->add('codePostal', TextType::class, array(
                'required' => false,
                'label' => 'Code postal',
                'attr' => array(
                    'pattern' => '[0-9]{5}',
                )
            ))
            ->add('ville')
            ->add('telephone', TelType::class, array(
                'label' => 'Téléphone',
                'required' => false,
                'attr' => array(
                    'pattern' => '^(?:0|\(?\+33\)?\s?|0033\s?)[1-79](?:[\.\-\s]?\d\d){4}$',
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'required' => false,
            ))
            ->add('dateNaissance', BirthdayType::class, array(
                'label' => 'Date de naissance',
                'required' => false,
                'html5' => false,
                //'format' => 'dd/MM/yyyy',
                'widget' => 'choice',
                //'format' => 'yyyy-MM-dd',
                'years' => range($this->minYear, $this->maxYear)

            ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Adherent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_adherent';
    }

    private function getChoices()
    {
        /*
        $choices = [
            "Adjoints administratifs de la fonction publique (y.c. enseignement)",
            "Adjudants-chefs adjudants et sous-officiers de rang supérieur de l'Armée et de la Gendarmerie",
            "Agents immobiliers indépendants"
        ];
        $output = [];
        foreach($choices as $k => $v) {
            $output[$v] = $v;
        }
        return $output;
        */
        $choices = array();
        $choices['-----'] = '';

        $professions = $this->entityManager->getRepository(Profession::class)->findAll();

        foreach ($professions as $profession) {
            $choices[$profession->getLibelle()] = $profession->getLibelle();
        }

        return $choices;
    }

}
