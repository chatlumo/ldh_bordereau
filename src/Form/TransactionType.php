<?php

namespace App\Form;

use App\Entity\Transaction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionType extends AbstractType
{
    protected $minYear, $maxYear;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $now = new \DateTime('now');
        $this->maxYear = (int) $now->format('Y');
        $this->minYear = $this->maxYear - 1;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adherent', AdherentType::class, array(
                'label' => 'Adhérent',

            ))
            ->add('cotisation', IntegerType::class, array(
                'label' => 'Cotisation en €',
                'required' => false,
                'attr' => array(
                    'min' => 10,
                )
            ))
            ->add('hlSubscription', CheckboxType::class, array(
                'label' => 'Abonnement à H&L ('.Transaction::TARIF_HL.' €)',
                'value' => Transaction::TARIF_HL,
                'required' => false,

            ))
            ->add('ldhInfoSubscription', CheckboxType::class, array(
                'label' => 'Abonnement à LDH Info ('.Transaction::TARIF_LDHINFO.' €)',
                'value' => Transaction::TARIF_LDHINFO,
                'required' => false,

            ))
            ->add('donSiege', IntegerType::class, array(
                'label' => 'Don pour le siège en €',
                'required' => false,
            ))
            ->add('donSection', IntegerType::class, array(
                'label' => 'Don pour la section en €',
                'required' => false,
            ))
            ->add('total', TextType::class, array(
                'required' => false,
                'label' => 'Total payé par l\'adhérent',
                'attr' => array(
                    'readonly' => true,
                )
            ))
            ->add('note', TextType::class, array(
                'required' => false,
                'attr' => array(
                    'maxlength' => 255,
                )
            ))
            ->add('paiementDate', DateType::class, array(
                'label' => 'Date de paiement',
                'required' => true,
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'widget' => 'single_text',
                //'format' => 'yyyy-MM-dd',
                'years' => range($this->minYear, $this->maxYear)

            ))
            ->add('paiementType', ChoiceType::class, array(
                'choices' => [
                    'Chèque' => 'CH',
                    'Espèces' => 'AC',
                    'Virement' => 'VIRE'
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => 'Moyen de paiement'
            ))
            ->add('paiementRef', TextType::class, array(
                'required' => false,
                'attr' => array(
                    'maxlength' => 30,
                ),
                'label' => 'N° du chèque (si chèque)'
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Transaction',
        ));
    }

}
