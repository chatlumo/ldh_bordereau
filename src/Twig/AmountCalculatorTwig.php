<?php

namespace App\Twig;


use App\Service\AmountCalculator;

class AmountCalculatorTwig extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            // the logic of this filter is now implemented in a different class
            new \Twig_SimpleFilter('partSiege', array(AmountCalculator::class, 'partSiege')),
            new \Twig_SimpleFilter('partSection', array(AmountCalculator::class, 'partSection')),
            new \Twig_SimpleFilter('totalCotisation', array(AmountCalculator::class, 'totalCotisation')),
            new \Twig_SimpleFilter('totalDonSiege', array(AmountCalculator::class, 'totalDonSiege')),
            new \Twig_SimpleFilter('totalLdhInfo', array(AmountCalculator::class, 'totalLdhInfo')),
            new \Twig_SimpleFilter('totalHlHT', array(AmountCalculator::class, 'totalHlHT')),
            new \Twig_SimpleFilter('totalHlTVA', array(AmountCalculator::class, 'totalHlTVA')),
        );
    }
}