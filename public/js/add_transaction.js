function ucwords (str) {
    strTemp = str + '';
    //strTemp = strTemp.replace('-', ' ');
    return strTemp
        .replace(/^(.)|\s+(.)|-+(.)/g, function ($1) {
            return $1.toUpperCase()
        })
}

$("#transaction_adherent_prenom").on('keyup blur', function () {
    $(this).val(ucwords($(this).val()));
});

$("#transaction_adherent_nom, #transaction_adherent_ville, #transaction_adherent_adresse").on('keyup blur', function () {
    $(this).val($(this).val().normalize('NFD').replace(/[\u0300-\u036f,]/g, "").toUpperCase());
});

$("#transaction_adherent_noAdh").on('keyup blur', function () {
    $(this).val($(this).val().replace(/^[0|\D]*/gm, "").replace(/[\D]*/gm, ""));
});

$("#transaction_cotisation, #transaction_donSiege, #transaction_donSection, #transaction_hlSubscription, #transaction_ldhInfoSubscription").change(function () {
    var prefix = '#transaction_';
    var cotisation = $(prefix + 'cotisation').val();
    var donSiege = $(prefix + 'donSiege').val();
    var donSection = $(prefix + 'donSection').val();
    var hl = $(prefix + 'hlSubscription').prop('checked') ? $(prefix + 'hlSubscription').val() : 0;
    var ldhInfo = $(prefix + 'ldhInfoSubscription').prop('checked') ? $(prefix + 'ldhInfoSubscription').val() : 0;
    var total = Number(cotisation) + Number(donSiege) + Number(donSection) + Number(hl) + Number(ldhInfo);
    $(prefix + 'total').val(total);
});

$("#removeModal, #envoiModal").on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
});


// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();